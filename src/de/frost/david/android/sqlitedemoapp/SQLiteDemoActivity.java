package de.frost.david.android.sqlitedemoapp;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

public class SQLiteDemoActivity extends SingleFragmentActivity {
	private SQLiteDemoFragment mSQLiteDemoFragment;

	public static final int REQUEST_HOUSE = 0;
	public static final int REQUEST_PERSON = 1;

	private static final String TAG = "SQLiteDemoActivity";

	@Override
	protected Fragment createFragment() {
		mSQLiteDemoFragment = new SQLiteDemoFragment();
		return mSQLiteDemoFragment;
	}

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i(TAG, "in SQLiteDemoActivity.onActivityResult");
		
		SQLiteDemoFragment fm = (SQLiteDemoFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
		fm.updateUI();
	}

}
