package de.frost.david.android.sqlitedemoapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import de.frost.david.android.sqlitedemoapp.PlacesDatabaseHelper.PersonCursor;

public class HouseFragment extends Fragment {

	public static final String RESULT_HOUSE = "de.frost.david.android.sqlitedemoapp.NEW_HOUSE";
	private static final String ARG_HOUSE_ID = "HOUSE_ID";
	private static final String TAG = "HouseFragment";

	private EditText mStreetEditText;
	private Button mSaveButton;
	private TextView mPersonsTextView;
	private ListView mPersonsList;
	private House mHouse;
	private DataModel mDataModel;
	private PersonCursor mCursor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		mDataModel = DataModel.getInstance(getActivity());

		Bundle args = getArguments();
		if (args != null) {
			long id = args.getLong(ARG_HOUSE_ID, -1);
			if (id != -1) {
				Log.i(TAG, "id in HouseFragment angekommen: " + id);
				mHouse = mDataModel.getHouse(id);
			}
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_house, container, false);

		mPersonsTextView = (TextView) v.findViewById(R.id.house_persons);
		mStreetEditText = (EditText) v.findViewById(R.id.addhouse_editText_street);
		mPersonsList = (ListView) v.findViewById(R.id.house_personslist);
		mSaveButton = (Button) v.findViewById(R.id.addhouse_button_save);

		mSaveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mStreetEditText.getText().toString() == null)
					return;

				if (mHouse != null) {
					mHouse.setStreet(mStreetEditText.getText().toString());
					mDataModel.updateHouse(mHouse, mHouse.getId());
				} else {
					mHouse = new House();
					mHouse.setStreet(mStreetEditText.getText().toString());
					mHouse.setId(mDataModel.insertHouse(mHouse));
				}

				Intent data = new Intent();
				data.putExtra(RESULT_HOUSE, mStreetEditText.getText().toString());
				getActivity().setResult(Activity.RESULT_OK, data);
				getActivity().finish();
			}
		});

		if (mHouse != null) {
			mStreetEditText.setText(mHouse.getStreet());

			mCursor = mDataModel.queryPersonsInHouse(mHouse.getId());
			mPersonsList.setAdapter(new PersonCursorAdapter(getActivity(), mCursor));
			if (mCursor.getCount() < 1) {
				mPersonsTextView.setText(getString(R.string.no_persons));
			}
			
		} else {
			mPersonsTextView.setText(getString(R.string.no_persons));
		}
		
		mPersonsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Person p = ((PersonCursor) mPersonsList.getAdapter().getItem(position)).getPerson();	
				Intent i = new Intent(getActivity(), PersonActivity.class);
				i.putExtra(PersonActivity.EXTRA_PERSON_ID, p.getId());
				startActivityForResult(i, SQLiteDemoActivity.REQUEST_PERSON);
			}
			
		});
		
		mPersonsList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				Toast.makeText(getActivity(), "Person got deleted!", Toast.LENGTH_SHORT).show();
				Person p = ((PersonCursor)(mPersonsList.getAdapter().getItem(position))).getPerson();
				mDataModel.deletePerson(p.getId());
				updateUI();
				return true;
			}
		});

		return v;
	}

	public static Fragment newInstance(long id) {
		Bundle args = new Bundle();
		args.putLong(ARG_HOUSE_ID, id);
		HouseFragment hf = new HouseFragment();
		hf.setArguments(args);
		return hf;
	}
	
	public void updateUI() {
		mCursor.requery();
		((PersonCursorAdapter)mPersonsList.getAdapter()).notifyDataSetChanged();
		if (mPersonsList.getCount() == 0) {
			mPersonsTextView.setText(R.string.no_persons);
		}
	}

	private static class PersonCursorAdapter extends CursorAdapter {

		private PersonCursor mPersonCursor;

		public PersonCursorAdapter(Context context, PersonCursor c) {
			super(context, c, 0);
			this.mPersonCursor = c;
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
		}

		@Override
		public void bindView(View v, Context c, Cursor cursor) {
			Person p = mPersonCursor.getPerson();

			TextView tv = (TextView) v;
			tv.setText(p.getId() + " / " + p.getName());
		}

	}

}
