package de.frost.david.android.sqlitedemoapp;

public class Person {
	private long mId;
	private String mName;
	private long mHouseId;
	
	public Person() {
		this.mId = -1;
	}
	
	public Person(String name, long house) {
		this();
		this.mName = name;
		this.mHouseId = house;
	}

	public long getId() {
		return mId;
	}

	public String getName() {
		return mName;
	}

	public long getHouseId() {
		return mHouseId;
	}

	public void setId(long id) {
		mId = id;
	}

	public void setName(String name) {
		mName = name;
	}

	public void setHouseId(long houseId) {
		mHouseId = houseId;
	}

}
