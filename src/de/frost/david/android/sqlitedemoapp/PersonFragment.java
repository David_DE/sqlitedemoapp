package de.frost.david.android.sqlitedemoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import de.frost.david.android.sqlitedemoapp.PlacesDatabaseHelper.HouseCursor;
import de.frost.david.android.sqlitedemoapp.SQLiteDemoFragment.HouseCursorAdapter;

public class PersonFragment extends Fragment {
	
	public static final String RESULT_PERSON = "de.frost.david.android.sqlitedemoapp.NEW_PERSON";
	private static final String ARG_PERSON_ID = "PERSON_ID";
	
	private DataModel mDataModel;
	private EditText mNameEditText;
	private Button mSaveButton;
	private Spinner mHouseSpinner;
	private HouseCursor mCursor;
	private TextView mHouseTextView;
	
	private Person mPerson;
	
	public static Fragment newInstance(long id) {
		Bundle args = new Bundle();
		args.putLong(ARG_PERSON_ID, id);
		PersonFragment pf = new PersonFragment();
		pf.setArguments(args);
		return pf;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		mDataModel = DataModel.getInstance(getActivity());

		Bundle args = getArguments();
		if (args != null) {
			long id = args.getLong(ARG_PERSON_ID, -1);
			if (id != -1) {
				mPerson = mDataModel.getPerson(id);
			}
		}
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_person, container, false);

		mNameEditText = (EditText) v.findViewById(R.id.person_editText_name);
		mHouseTextView = (TextView)v.findViewById(R.id.person_livesIn);
		mSaveButton = (Button) v.findViewById(R.id.person_button_save);
		mSaveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mNameEditText.getText().toString() == null)
					return;

				if (mPerson != null) {
					mPerson.setName(mNameEditText.getText().toString());
					mPerson.setHouseId(getHouseIdFromSpinnner());
					mDataModel.updatePerson(mPerson, mPerson.getId());
				} else {
					mPerson = new Person();
					mPerson.setName(mNameEditText.getText().toString());
					mPerson.setHouseId(getHouseIdFromSpinnner());
					mPerson.setId(mDataModel.insertPerson(mPerson));
				}

				Intent data = new Intent();
				data.putExtra(RESULT_PERSON, mNameEditText.getText().toString());
				getActivity().setResult(Activity.RESULT_OK, data);
				getActivity().finish();
			}
		});
		
		mHouseSpinner = (Spinner) v.findViewById(R.id.person_spinner_houses);
		mCursor = mDataModel.queryHouse();
		HouseCursorAdapter adapter = new HouseCursorAdapter(getActivity(), mCursor);
		mHouseSpinner.setAdapter(adapter);
		

		if (mPerson != null) {
			mNameEditText.setText(mPerson.getName());
			
			HouseCursorAdapter a = (HouseCursorAdapter) mHouseSpinner.getAdapter();
			
			for (int i = 0; i < a.getCount(); i++) {
				HouseCursor hc = (HouseCursor) a.getItem(i);
				
				if (hc.getHouse().getId() == mPerson.getHouseId()) {
					mHouseSpinner.setSelection(i);
					break;
				}
			}
			
		} else {
			mHouseTextView.setText(R.string.choose);
		}

		return v;
	}
	
	private long getHouseIdFromSpinnner() {
		House h = ((HouseCursor) mHouseSpinner.getSelectedItem()).getHouse();
		return h.getId();
	}

}
