package de.frost.david.android.sqlitedemoapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PlacesDatabaseHelper extends SQLiteOpenHelper {
	private static final String DB_NAME = "places.sqlite";
	private static final int VERSION = 1;

	private static final String TABLE_HOUSE = "house";
	private static final String COLUMN_HOUSE_ID = "_id";
	private static final String COLUMN_HOUSE_STREET = "street";

	private static final String TABLE_PERSON = "person";
	private static final String COLUMN_PERSON_ID = "_id";
	private static final String COLUMN_PERSON_NAME = "name";
	private static final String COLUMN_PERSON_HOUSE_ID = "house_id";
	private static final String TAG = "PlacesDatabaseHelper";

	public PlacesDatabaseHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL("CREATE TABLE " + TABLE_HOUSE + " (" 
				+ COLUMN_HOUSE_ID + " integer primary key autoincrement, " 
				+ COLUMN_HOUSE_STREET + " varchar(100))");

		db.execSQL("CREATE TABLE " + TABLE_PERSON + " (" 
				+ COLUMN_PERSON_ID + " integer primary key autoincrement, " 
				+ COLUMN_PERSON_NAME + " varchar(100), " 
				+ COLUMN_PERSON_HOUSE_ID + " integer references house(_id))");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.rawQuery("DROP DATABASE " + DB_NAME, null);
		onCreate(db);
	}

	public long insertHouse(House h) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_HOUSE_STREET, h.getStreet());

		return getWritableDatabase().insert(TABLE_HOUSE, null, cv);
	}

	public long insertPerson(Person p) {
		ContentValues cv = new ContentValues();

		cv.put(COLUMN_PERSON_NAME, p.getName());
		cv.put(COLUMN_PERSON_HOUSE_ID, p.getHouseId());

		return getWritableDatabase().insert(TABLE_PERSON, null, cv);
	}
	
	public HouseCursor queryHouse() {
		Cursor wrapped = getReadableDatabase().query(TABLE_HOUSE, null, null, null, null, null, null);
		Log.i(TAG, "Anzahl rows: " + wrapped.getCount());
		return new HouseCursor(wrapped);
	}
	
	public PersonCursor queryPerson() {
		Cursor wrapped = getReadableDatabase().query(TABLE_PERSON, null, null, null, null, null, null);
		return new PersonCursor(wrapped);
	}
	
	public House queryHouse(long id) {
		Cursor wrapped = getReadableDatabase().query(TABLE_HOUSE, null, "_id=?", new String[]{""+id}, null, null, null);
		HouseCursor hc = new HouseCursor(wrapped);
		
		hc.moveToFirst();
		House h = hc.getHouse();
		hc.close();
		return h;
	}
	
	public Person queryPerson(long id) {
		Cursor wrapped = getReadableDatabase().query(TABLE_PERSON, null, "_id=?", new String[]{""+id}, null, null, null);
		PersonCursor pc = new PersonCursor(wrapped);
		
		pc.moveToFirst();
		Person p = pc.getPerson();
		pc.close();
		return p;
	}
	

	public void updateHouse(House h, long id) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_HOUSE_STREET, h.getStreet());
		getWritableDatabase().update(TABLE_HOUSE, cv, "_id=?", new String[]{""+id});
	}
	
	public void updatePerson(Person person, long houseId) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_PERSON_NAME, person.getName());
		cv.put(COLUMN_PERSON_HOUSE_ID, person.getHouseId());
		getWritableDatabase().update(TABLE_PERSON, cv, "_id=?", new String[]{""+houseId});
	}
	
	public void deleteHouse(long id) {
		getWritableDatabase().delete(TABLE_HOUSE, "_id=?", new String[]{""+id});
	}
	

	public void deletePerson(long id) {
		getWritableDatabase().delete(TABLE_PERSON, "_id=?", new String[]{""+id});
	}
	
	public void deletePersonsInHouse(long hid) {
		getWritableDatabase().delete(TABLE_PERSON, "house_id=?", new String[]{""+hid});		
	}
	
	public PersonCursor queryPersonsInHouse(long id) {
		Cursor wrapped = getReadableDatabase().query(TABLE_PERSON, null, "house_id=?", new String[]{""+id}, null, null, null);
		return new PersonCursor(wrapped);
	}

	public static class HouseCursor extends CursorWrapper {

		public HouseCursor(Cursor cursor) {
			super(cursor);
		}

		public House getHouse() {
			if (isBeforeFirst() || isAfterLast())
				return null;

			House h = new House();
			h.setId(getLong(getColumnIndex(COLUMN_HOUSE_ID)));
			h.setStreet(getString(getColumnIndex(COLUMN_HOUSE_STREET)));

			Log.i(TAG, "in getHouse()" + h.getStreet());
			return h;
		}
		
//		public int getPosition() {
//			
//		}

	}

	public static class PersonCursor extends CursorWrapper {

		public PersonCursor(Cursor cursor) {
			super(cursor);
		}

		public Person getPerson() {
			if (isBeforeFirst() || isAfterLast())
				return null;

			Person p = new Person();

			p.setId(getLong(getColumnIndex(COLUMN_PERSON_ID)));
			p.setName(getString(getColumnIndex(COLUMN_PERSON_NAME)));
			p.setHouseId(getLong(getColumnIndex(COLUMN_PERSON_HOUSE_ID)));

			return p;
		}

	}






}
