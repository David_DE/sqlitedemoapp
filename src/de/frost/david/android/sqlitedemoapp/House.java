package de.frost.david.android.sqlitedemoapp;

public class House {
	private long mId;
	private String mStreet;
	
	public House() {
		this.mId = -1;
	}
	
	public House(String street) {
		this.mId = -1;
		this.mStreet = street;
	}

	public long getId() {
		return mId;
	}

	public void setId(long id) {
		mId = id;
	}

	public void setStreet(String street) {
		mStreet = street;
	}

	public String getStreet() {
		return this.mStreet;
	}

}
