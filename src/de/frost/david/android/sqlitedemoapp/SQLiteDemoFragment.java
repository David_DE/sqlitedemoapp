package de.frost.david.android.sqlitedemoapp;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import de.frost.david.android.sqlitedemoapp.PlacesDatabaseHelper.HouseCursor;

public class SQLiteDemoFragment extends ListFragment {
//	private static final String TAG = "SQLiteDemoFragment";
	private DataModel mDataModel;
	private HouseCursor mCursor;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_sqlite_demo, parent, false);
		
		
		mDataModel = DataModel.getInstance(getActivity());

		//TEMP STUFF
//		getActivity().deleteDatabase("places.sqlite");
//		House h1 = new House("Whatever Street");
//		h1.setId(sql.insertHouse(h1));
//		House h2 = new House("Nowhere Street");
//		h2.setId(sql.insertHouse(h2));
//		House h3 = new House("There Street");
//		h3.setId(sql.insertHouse(h3));
		
//		Person p1 = new Person("Tom", 1);
//		Person p2 = new Person("Dieter", 2);
//		Person p3 = new Person("Karsten", 1);
//		Person p4 = new Person("Hans-Peter", 1);
//		mDataModel.insertPerson(p1);
//		mDataModel.insertPerson(p2);
//		mDataModel.insertPerson(p3);
//		mDataModel.insertPerson(p4);
		
		ListView lv = (ListView) v.findViewById(android.R.id.list);
		registerForContextMenu(lv);
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				Toast.makeText(getActivity(), "House got deleted!", Toast.LENGTH_SHORT).show();
				House h = ((HouseCursor)(getListAdapter().getItem(position))).getHouse();
				mDataModel.deletePersonsInHouse(h.getId());
				mDataModel.deleteHouse(h.getId());
				updateUI();
				return true;
			}
		});
		
		
		mCursor = mDataModel.queryHouse();
		HouseCursorAdapter adapter = new HouseCursorAdapter(getActivity(), mCursor);
		setListAdapter(adapter);
		return v;
	}
	
	@Override
    public void onListItemClick(ListView l, View v, int position, long id) {
		Intent i = new Intent(getActivity(), HouseActivity.class);
		
		House h = ((HouseCursor)(getListAdapter().getItem(position))).getHouse();
		long idd = h.getId();
		
		i.putExtra(HouseActivity.EXTRA_HOUSE_ID, idd);
		getActivity().startActivityForResult(i, SQLiteDemoActivity.REQUEST_HOUSE);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_sqlitedemoapp, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_addHouse:
			Intent i = new Intent(getActivity(), HouseActivity.class);
			getActivity().startActivityForResult(i, SQLiteDemoActivity.REQUEST_HOUSE);
			return true;
		case R.id.menu_addPerson:
			Intent ip = new Intent(getActivity(), PersonActivity.class);
			getActivity().startActivityForResult(ip, SQLiteDemoActivity.REQUEST_PERSON);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void updateUI() {
		mCursor.requery();
		((HouseCursorAdapter)getListAdapter()).notifyDataSetChanged();
	}
	
	public static class HouseCursorAdapter extends CursorAdapter {
		
		private HouseCursor mHouseCursor;

		public HouseCursorAdapter(Context context, HouseCursor c) {
			super(context, c, 0);
			this.mHouseCursor = c;
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
		}
		
		@Override
		public void bindView(View v, Context c, Cursor cursor) {
			House h = mHouseCursor.getHouse();
			
			TextView tv = (TextView)v;
			tv.setText(h.getId() + " / " + h.getStreet());
		}

		
	}
	

}
