package de.frost.david.android.sqlitedemoapp;

import android.support.v4.app.Fragment;

public class PersonActivity extends SingleFragmentActivity {
	public static final String EXTRA_PERSON_ID = "EXTRA_PERSON_ID";

	@Override
	protected Fragment createFragment() {
		long id = getIntent().getLongExtra(EXTRA_PERSON_ID, -1);
		if (id != -1) {
			return PersonFragment.newInstance(id);
		} else {
			return new PersonFragment();			
		}
	}

}
