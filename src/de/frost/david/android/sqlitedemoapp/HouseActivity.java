package de.frost.david.android.sqlitedemoapp;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

public class HouseActivity extends SingleFragmentActivity {
	public static final String EXTRA_HOUSE_ID = "EXTRA_HOUSE_ID";
	private static final String TAG = "HouseActivity";

	@Override
	protected Fragment createFragment() {
		long id = getIntent().getLongExtra(EXTRA_HOUSE_ID, -1);
		if (id != -1) {
			Log.i(TAG, "id in HouseActivity angekommen: " + id);
			return HouseFragment.newInstance(id);
		} else {
			return new HouseFragment();			
		}
		
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i(TAG, "in HouseActivity.onActivityResult");
		
		HouseFragment hm = (HouseFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
		hm.updateUI();
	}

}
