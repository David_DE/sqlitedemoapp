package de.frost.david.android.sqlitedemoapp;

import java.util.ArrayList;

import de.frost.david.android.sqlitedemoapp.PlacesDatabaseHelper.HouseCursor;
import de.frost.david.android.sqlitedemoapp.PlacesDatabaseHelper.PersonCursor;
import android.content.Context;

public class DataModel {
	private static DataModel mDataModel;
	private PlacesDatabaseHelper mSQLHelper;
	private ArrayList<House> mHouses;
	private ArrayList<Person> mPersons;

	public static DataModel getInstance(Context c) {
		if (mDataModel == null) {
			mDataModel = new DataModel(c);
		}
		
		return mDataModel;
	}
	
	private DataModel (Context c) {
		mSQLHelper = new PlacesDatabaseHelper(c);
		mHouses = new ArrayList<House>();
		mPersons = new ArrayList<Person>();
	}
	
	public HouseCursor queryHouse() {
		return mSQLHelper.queryHouse();
	}
	
	public PersonCursor queryPerson() {
		return mSQLHelper.queryPerson();
	}
	
	
	public long insertHouse(House h) {
		h.setId(mSQLHelper.insertHouse(h));
		mHouses.add(h);
		return h.getId();
	}
	
	public long insertPerson(Person p) {
		p.setId(mSQLHelper.insertPerson(p));
		mPersons.add(p);
		return p.getId();
	}
	

	public House getHouse(long id) {
		return mSQLHelper.queryHouse(id);
	}
	
	public Person getPerson(long id) {
		return mSQLHelper.queryPerson(id);
	}
	
	
	public void updateHouse(House h, long id) {
		mSQLHelper.updateHouse(h, id);
	}
	
	public void updatePerson(Person person, long personId) {
		mSQLHelper.updatePerson(person, personId);
	}
	
	
	public void deleteHouse(long id) {
		mSQLHelper.deleteHouse(id);
	}
	
	public void deletePerson(long id) {
		mSQLHelper.deletePerson(id);
	}
	
	
	public PersonCursor queryPersonsInHouse(long id) {
		return mSQLHelper.queryPersonsInHouse(id);
	}
	
	public void deletePersonsInHouse(long hid) {
		mSQLHelper.deletePersonsInHouse(hid);
	}

	public House queryHouseOfPerson(long id) {
		return getHouse(getPerson(id).getHouseId());
	}
	

}
