SQLiteDemoApp
==============

Just a demo to try out SQLite on Android. 
There are person and house objects. Houses have an Id as well as a street name (more would off course be possible) and persons have an id , a name and a house id where they live.
Everything is stored in two SQL tables and can get updated or deleted from there.


Used classes:
==============
- Activity/ListFragment/Fragment: For basic app
- SQLiteOpenHelper: Creates the database, the tables and is used to insert or select data
- CursorAdapter/Cursor: To fill the ListView with content from SQLite

ToDo
==============
- ?? 